FROM debian:stable-slim

RUN apt-get update \
  && apt-get install -y wget file fuse libfuse-dev gpg appstream

RUN mkdir /appimagetool
WORKDIR /appimagetool

COPY appimagetool.sh .
RUN wget "https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
RUN chmod a+x appimagetool-x86_64.AppImage
